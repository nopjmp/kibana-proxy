/**
 * Configure bitbucket oauth passport's.
 * Config parameters:
 * - consumerKey: the consumer key
 * - consumerSecret: the consumer secret
 * - team: team to check for membership
 * When no consumerKey or consumerSecret, no oauth config takes place.
 */

exports.configureOAuth = function(express, app, config) {
  if (!config.consumerKey || !config.consumerSecret || !config.team) {
    console.log('not setup of bitbucket oauth');
    return;
  }

  var request = require('request');
  var team_url = 'https://bitbucket.org/api/2.0/teams/' + config.team + '/members'

  var BitbucketStrategy = require('passport-bitbucket').Strategy;
  var passport = require('passport');
  var passportIsSet = false;

  var lazySetupPassport = function(req) {
    passportIsSet = true;

    var protocol = (req.connection.encrypted || req.headers['x-forwarded-proto'] === "https" ) ? "https" : "http";

  //not doing anything with this:
  //it will try to serialize the users in the session.
    passport.serializeUser(function(user, done) {
      done(null, user);
    });
    passport.deserializeUser(function(obj, done) {
      done(null, obj);
    });

    var callbackUrl = protocol + "://" + req.headers.host + "/auth/bitbucket/callback";
    passport.use(new BitbucketStrategy({
      consumerKey: config.consumerKey,
      consumerSecret: config.consumerSecret,
      callbackURL: callbackUrl
    }, function(accessToken, refreshToken, profile, done) {
      request.get(team_url, function(error, res, body) {
        if(!error && res.statusCode == 200) {
          var members = JSON.parse(body).values;
          for (m in members) {
            if (profile.username === members[m].username) {
              return done(null, profile);
            }
          }
          return done(null, false, { message: 'not an authorized username ' + profile.username });
        }
        done(null, false, { message: 'error retrieving team (bitbucket down?)'});
      });
    }));

    app.get('/auth/bitbucket', passport.authenticate('bitbucket'));

    app.get('/auth/bitbucket/callback', passport.authenticate('bitbucket'
    , { failureRedirect: '/auth/bitbucket/fail' })
    , function(req, res) {
      // Successful authentication, redirect home.
      req.session.authenticated = true;
      res.redirect(req.session.beforeLoginURL || '/');
    });

    app.get('/auth/bitbucket/fail', function(req, res) {
      res.send('not permitted to view content from this bitbucket account');
    });
  };

  app.use(express.bodyParser());
  app.use(require('connect-restreamer')());
  app.use(express.cookieParser());
  //replacement for bodyParser that is compatible with htt-proxy if we must have one (but we don't)
  app.use(express.session({ secret: config.secret }));
  app.use(function(req, res, next) {
    if (req.isAuthenticated() ||
        req.url.indexOf('/auth/bitbucket') === 0 ||
        req.session.authenticated ||
        req.url === '/healthcheck') {
      return next();
    }
    if (!passportIsSet) {
      lazySetupPassport(req);
    }

    req.session.beforeLoginURL = req.url;
    res.redirect('/auth/bitbucket');
  });
  app.use(passport.initialize());

};
